package athena.studio.utils.terminal;

import java.util.Scanner;

public class UIController {
	public Scanner scanner = new Scanner(System.in);
	
	public UIController() {
		
	}
	
	public void printRegionSelection() {
		System.out.println("\n\nSelect your Region : ");
		System.out.println("\t1. United State ");
		System.out.println("\t2. Europe ");
		System.out.println("\t3. Viet Nam ");
		System.out.print("Type in your number : ");
	}
	
	public String getInputRegion() {
		String input = scanner.nextLine();
		
		switch (Integer.parseInt(input)) {
		case 1:
			return "US";
		case 2:
			return "EU";
		case 3:
			return "VN";
		default:
			return null;
		}
	}
	
	public void printFactoryAction() {
		System.out.println("\n\nSelect your action : ");
		System.out.println("\t1. Add User ");
		System.out.println("\t2. Add point to User ");
		System.out.println("\t3. Undo last action ");
		System.out.println("\t4. Show all existing Users ");
		System.out.println("\t5. Back to region selection ");
		System.out.println("Type in your number : ");
	}
	
	public int getInputFactoryAction() {
		String input = scanner.nextLine();
		int return_data = Integer.parseInt(input); 
		return  return_data -  1 > 4 ? null : return_data;
	}
	
	public void printNameRequest() {
		System.out.println("Input your name : ");
	}
	
	public void printPointRequest() {
		System.out.println("Input Point (Ex : 100.0, 200.5 ...) : ");
	}
	
	public String getName() {
		return scanner.nextLine();
	}
	
	public Double getPoint() {
		String input = scanner.nextLine();
		return (double) Float.parseFloat(input);
	}
	
}

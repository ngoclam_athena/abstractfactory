package studio.athena.superfactory;

import java.util.HashMap;

import studio.athena.abstractfactory.RegionAbstractFactory;
import studio.athena.abstractfactory.RegionAbstractFactory.Memento;
import studio.athena.abstractfactory.imp.EUUserFactory;
import studio.athena.abstractfactory.imp.USUserFactory;
import studio.athena.abstractfactory.imp.VNUserFactory;
import studio.athena.decoratorfactory.DecoratorFactory;

public class UserCreator {
	private UserCreator() {
	}
	
	public static DecoratorFactory getFactory(String region_type) {
		switch (region_type) {
		case "US" :
			return new DecoratorFactory(new USUserFactory());
		case "EU" :
			return new DecoratorFactory(new EUUserFactory());
		case "VN" :
			return new DecoratorFactory(new VNUserFactory());
		default:
            throw new UnsupportedOperationException("This region is unsupported ");
		}
	}
}

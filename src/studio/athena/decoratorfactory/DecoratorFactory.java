package studio.athena.decoratorfactory;

import java.util.Set;

import studio.athena.abstractfactory.RegionAbstractFactory;
import studio.athena.abstractfactory.RegionAbstractFactory.Memento;
import studio.athena.abstractuser.User;

public final class DecoratorFactory {
	public RegionAbstractFactory regionFactory;
	public DecoratorFactory(RegionAbstractFactory factory) {
		regionFactory = factory;
	}
	
	public Memento createUser(String name) {
		regionFactory.createUser(name);
		Memento checkPoint = regionFactory.saveToMemento();
		return checkPoint;
	}
	
	public Memento addPoint(String name, double point) {
		regionFactory.addPoint(name, point);
		Memento checkPoint = regionFactory.saveToMemento();
		return checkPoint;
	}
	
	public User getUser(String name) {
		User user = regionFactory.getUser(name);
		return user;
	}
		
	public void restoreFromMemento(Memento checkPoint) {
		regionFactory.restoreFromMemento(checkPoint);
	}
	
	public void showAllUsers() {
		Set<String> keys = regionFactory.userStorage.keySet();
		System.out.println("START SHOWING USERS : ");
        for(String key: keys){
            System.out.print(key + "\t");
        }
        System.out.println("\nEND");
	}
}

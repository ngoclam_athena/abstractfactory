package studio.athena.abstractuser;

public abstract class User {
	
	protected Double current_point;
	protected String name;
	protected Double goldThresh = 100.0;
	protected Double plantinumThresh = 500.0;
	protected String userType = "Silver";
	
	public User initialize(String name, Double point) {
		this.name = name;
		this.current_point = point;
		if (this.current_point > plantinumThresh) {
			userType = "Plantinum";
		}else if (this.current_point > goldThresh) {
			userType = "Gold";
		} else {
			userType = "Silver";
		}
		return this;
	}
	
	public void getUserType() {
		System.out.format("Plantinum thresh : %.2f, Gold thresh : %.2f\n", plantinumThresh, goldThresh);
		System.out.format("User %s has %.2f points and is %s\n", this.name, this.current_point, userType);
	}
}

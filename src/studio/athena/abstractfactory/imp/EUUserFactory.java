package studio.athena.abstractfactory.imp;


import studio.athena.abstractfactory.RegionAbstractFactory;
import studio.athena.abstractuser.imp.EUUser;

public class EUUserFactory extends RegionAbstractFactory {

	public EUUserFactory() {
		scale_factor = 1;
		this.userBluePrint = new EUUser();
	}
}

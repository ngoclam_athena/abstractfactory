package studio.athena.abstractfactory.imp;

import studio.athena.abstractfactory.RegionAbstractFactory;
import studio.athena.abstractuser.imp.USUser;

public class USUserFactory extends RegionAbstractFactory {

	
	
	public USUserFactory() {
		scale_factor = 1.0;
		this.userBluePrint = new USUser();
	}
}

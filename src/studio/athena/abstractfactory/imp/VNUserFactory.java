package studio.athena.abstractfactory.imp;


import studio.athena.abstractfactory.RegionAbstractFactory;
import studio.athena.abstractuser.imp.VNUser;

public class VNUserFactory extends RegionAbstractFactory {

	public VNUserFactory() {
		scale_factor = 1.5;
		this.userBluePrint = new VNUser();
	}
}

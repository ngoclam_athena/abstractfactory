package studio.athena.abstractfactory;

import java.util.HashMap;

import studio.athena.abstractuser.User;

public abstract class RegionAbstractFactory {
	
	protected double scale_factor;
	public HashMap<String, Double> userStorage = new HashMap<String, Double>() ;
	public User userBluePrint;
	
	
	public void createUser(String name) {
		userStorage.put(name, 0.0);
	}
	
	public void addPoint(String name, double point) {
		if (userStorage.containsKey(name)) {
			userStorage.put(name, userStorage.get(name) + point*scale_factor);			
		}else {
			System.out.println("THIS USER IS NOT EXISTED");
		}
		
	}
	
	public User getUser(String name) {
		if (userStorage.containsKey(name)) {
			
		}else {
			return null; 
		}
		return userBluePrint.initialize(name, userStorage.get(name));
	}
	
	public class Memento{
		private final HashMap<String, Double> userStorage;
		public Memento(HashMap<String, Double> storage){
			userStorage = (HashMap<String, Double>) storage.clone();
		}
		public HashMap<String, Double> get_saved_state(){
			return userStorage;
		}
	}
	
	public Memento saveToMemento() {
		return new Memento(userStorage);
	}
	
	public void restoreFromMemento(Memento checkPoint) {
		userStorage = checkPoint.get_saved_state();
	}
	
}

package studio.athena.abstractfactory;

import java.util.HashMap;
import java.util.LinkedList;

import athena.studio.utils.terminal.UIController;
import studio.athena.abstractfactory.RegionAbstractFactory.Memento;
import studio.athena.abstractuser.User;
import studio.athena.decoratorfactory.DecoratorFactory;
import studio.athena.superfactory.UserCreator;

public class ClientController {
	
	UIController controller = new UIController();
	DecoratorFactory userCreator = null;
	HashMap<String, LinkedList<Memento>> checkPoints = new HashMap<String, LinkedList<Memento>>();
	HashMap<String, Memento> currentState = new HashMap<String, Memento>();
	String region;
	
	public ClientController() {
		checkPoints.put("US", new LinkedList<Memento>());
		checkPoints.put("EU", new LinkedList<Memento>());
		checkPoints.put("VN", new LinkedList<Memento>());
	}
	
	public void run() {
		while (true) {
			getDecoratorFactory();
			int option = getActionOption();
			processActionOption(option);
		}
	}
	
	private void get_type(User user_1) {
		if (user_1 != null) {
			user_1.getUserType();			
		}else {
			System.out.println("User is not existed");
		}
	}
	
	private void getDecoratorFactory() {
		if (userCreator == null) {			
			controller.printRegionSelection();
			String option = controller.getInputRegion();
			region = option;
			userCreator = UserCreator.getFactory(option);
			loadCurrentState();
		}
	}
	
	private int getActionOption() {
		controller.printFactoryAction();
		int option = controller.getInputFactoryAction();
		return option;
	}
	
	private void processActionOption(int option) {
		switch (option) {
		case 1 : createUser(); break;
		case 2 : addPoint(); break;
		case 3 : undoLastAction(); break;
		case 4 : showAllExistingUser(); break;
		case 5 : userCreator = null; break;
		default : System.out.println("INVALID OPTION, PLEASE TRY AGAIN"); break;
		}
	}
	
	private void createUser() {
		controller.printNameRequest();
		String name = controller.getName();
		Memento newCheckpoint = userCreator.createUser(name);
		saveCheckPointAndState(newCheckpoint);
	}
	
	private void addPoint() {
		controller.printNameRequest();
		String name = controller.getName();
		controller.printPointRequest();
		Double point = controller.getPoint();
		Memento newCheckpoint = userCreator.addPoint(name, point);
		saveCheckPointAndState(newCheckpoint);
	}
	
	private void undoLastAction() {
		try {			
			Memento checkpoint = checkPoints.get(region).pollLast();
			userCreator.restoreFromMemento(checkpoint);
			currentState.put(region, checkpoint);
		} catch (Exception e) {
			System.out.println("NO PREVIOUS ACTION TO UNDO");
		}
	}
	
	private void loadCurrentState() {
		try {
			Memento checkpoint = currentState.get(region);
			userCreator.restoreFromMemento(checkpoint);
		} catch (Exception e) {
			System.out.println("NO CURRENT STATE TO LOAD");
		}
	}
	
	private void saveCheckPointAndState(Memento newstate) {
		try {
			if (currentState.get(region) != null) {				
				checkPoints.get(region).add(currentState.get(region));
			}
		} catch (Exception e) {
			System.out.println("SAVE CURRENT STATE ONLY");
		}
		currentState.put(region, newstate);
	}
	
	private void showAllExistingUser() {
		int i = 0;
		while (i < 2) {
			userCreator.showAllUsers();
			System.out.println("Type in name to check ranking or press ENTER to continue : ");
			String name = controller.getName();
			System.out.println(name);
			if (name.equals("")) {
				break;
			} else {
				i ++;
				User user = userCreator.getUser(name);
				if (user == null) {
					System.out.println("User " + name + " not existed");
				} else {
					user.getUserType();
				}
			}
		}
	}
	
}

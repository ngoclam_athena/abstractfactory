package studio.athena.abstractfactory;

public class Client {
	
	
	public static void main(String[] args) {
		ClientController controller = new ClientController();
		
		controller.run();
	}
}
